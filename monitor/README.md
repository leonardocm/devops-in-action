# Adicionar o Monitoramento

Vamos configurar o painel de monitoramento, vamos precisar mudar a configuração e disponibilizar os serviços e domínios diferentes. A aplicação vai ficar no domínio `app.devops.prod` e o de monitoramento ficará em `admin.devops.prod`.

## Exercício

Revise o playbook, os arquivos de configuração e aponte as alterações realizadas nos arquivos e depois execute o novo playbook e teste as novas configurações do servidor.

> ansible-playbook -i hosts app.yml

- Testar se a aplicação ficou disponível acessando `http://app.devops.prod/`

- Para testar o monitoramento tente: `http://admin.devops.prod/appmetrics-dash`
